package org.CsanyiZoltan.bmicalculator;

public class Main {

	public static void main(String[] args) {
		BMI test = new BMI();
		float h = test.enterHeight();
		float w = test.enterWeight();
		float bmi = test.calculateBMI(h, w);
		test.categoriesBMI(bmi);
	}

	
}
