package org.CsanyiZoltan.bmicalculator;

import java.util.Scanner;

public class BMI {
	public float enterHeight() {
		float height = 0;
		int choice;
		boolean notValid = true;
		Scanner sc = new Scanner(System.in);
		while (notValid) {
			System.out.println("Please select the height unit:");
			System.out.println("1 - milimeter");
			System.out.println("2 - centimeter");
			System.out.println("3 - meter");
			choice = sc.nextInt();
			System.out.println("Please enter the height ");
			height = sc.nextFloat();
			switch (choice) {
			case 1:
				height = height / 1000;
				break;
			case 2:
				height = height / 100;
				break;
			case 3:
				break;
			}
			if (height > 0.7 && height < 2.5) {
				notValid = false;
			} else {
				System.out.println("You entered an invalid number, please try it again");
				System.out.println();
			}
		}
		return height;
	}
	public float enterWeight() {
		float weight = 0;
		int choice;
		boolean notValid = true;
		Scanner sc = new Scanner(System.in);
		while (notValid) {
			System.out.println("Please select the weight unit:");
			System.out.println("1 - gram");
			System.out.println("2 - decagram");
			System.out.println("3 - kilogram");
			choice = sc.nextInt();
			System.out.println("Please enter the weight");
			weight = sc.nextFloat();
			switch (choice) {
			case 1:
				weight = weight / 1000;
				break;
			case 2:
				weight = weight / 100;
				break;
			case 3:
				break;
			}
			if (weight > 40 && weight < 200) {
				notValid = false;
			} else {
				System.out.println("You entered an invalid number, please try it again");
				System.out.println();
			}
		}
		return weight;
	}
	

	public float calculateBMI(float h, float w) {
		float height = h;
		float weight = w;
		float bmi = weight / (height * height);
		System.out.println("Your BMI is " + bmi );
		return bmi;
	}
	
	public String categoriesBMI(float index) {
		float bmi = index;
		String result;
		if(bmi>0 && bmi<16) {
			result = "Severe Thinnes";
		}else if(bmi>=16 && bmi<17) {
			result = "Moderate Thinnes";
		}else if(bmi>=17 && bmi<18.5) {
			result = "Mild Thinnes";
		}else if(bmi>=18.5 && bmi<25) {
			result = "Normal";
		}else if(bmi>=25 && bmi<30) {
			result = "Overweight";
		}else if(bmi>=30 && bmi<35) {
			result = "Obese Class I";
		}else if(bmi>=35 && bmi<40) {
			result = "Obese Class II";
		}else {
			result = "Obese Class III";
		}
		System.out.println("You are in the "+ result + " BMI category");
		return result;
	}
	
	
	
	
	
}
