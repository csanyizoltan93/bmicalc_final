package org.CsanyiZoltan.bmicalculator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CatTest {

	@Test
	public void test() {
		BMI cat = new BMI();
		String result = cat.categoriesBMI(25);
		assertEquals("Overweight", result);
	}

}
