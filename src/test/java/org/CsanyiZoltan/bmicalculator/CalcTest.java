package org.CsanyiZoltan.bmicalculator;

import org.junit.Test;

import junit.framework.TestCase;

public class CalcTest extends TestCase {
	
	
	@Test
	public void test() {
		BMI bmi= new BMI();
		float w = 60;
		float h = (float) 1.8;
		float result = bmi.calculateBMI(h, w);
		assertEquals(18.51852, result, 0.000001);
	}

}
